"use strict"

!(function() {
  let EpicApplication= {
    imgList: [],
    imgType:null,
    imageCache: {}
  };

  const imgTypes=["natural","enhanced","aerosol","cloud"];
  document.addEventListener("DOMContentLoaded", function(e){
      for (let i in imgTypes) {
        EpicApplication[imgTypes[i]]=null;
        EpicApplication.imageCache[imgTypes[i]]=new Map();
      }
      
      let totalDates=[];
      fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.imgType=document.querySelector("#type").value}/all`, {})
      .then(response => {  
        if(!response.ok) {
          throw new Error("Not 2xx response", {cause: response})
        }
        return response.json();
      })
      .then( obj => {
        totalDates=obj;
        totalDates.sort()
        let date=document.querySelector("#date");
        date.max=totalDates[0].date;
      })
    let submit=document.getElementById("request_form");
    let objList=[];
    let allDates=[];
    let template=document.querySelector("#image-menu-item");
    let ul=document.querySelector("#image-menu");
    let select=document.querySelector("#type");
    let date=document.querySelector("#date");

    submit.addEventListener("submit", function(e) { 
    EpicApplication.imgType=document.querySelector("#type").value;
     const existingDates = document.getElementById("image-menu").querySelectorAll("li");
      

      let imgDate=document.querySelector("#date").value;
      e.preventDefault();

        if (existingDates) {
          ul.textContent="";
          EpicApplication.imgList=[];
        }
        fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.imgType}/date/${imgDate}`, {})
        .then(response => {  
          if(!response.ok) {
            throw new Error("Not 2xx response", {cause: response})
          }
          return response.json();
        })
        .then( obj => {
          if(!EpicApplication.imageCache[EpicApplication.imgType].get(imgDate)) {
          let pieces;
          let dates=[];
            if(obj.length===0){
              let template_cloneNoImage=template.content.cloneNode(true).querySelector("li");
              template_cloneNoImage.textContent="No images found for this date"
              ul.appendChild(template_cloneNoImage);
            }
            objList=obj;
            for(let obj in objList) {
              EpicApplication.imgList.push(objList[obj].image); 
              let template_cloneLi=template.content.cloneNode(true).querySelector("li");

              let template_cloneSpan=template.content.cloneNode(true).querySelector("li").querySelector("span");
              template_cloneSpan.textContent=objList[obj].date;

              pieces=objList[obj].date.split(' ');
              dates[obj]=pieces[1];

              template_cloneSpan.setAttribute("data-index",obj);
              ul.appendChild(template_cloneLi);
              template_cloneLi.appendChild(template_cloneSpan);
            }
            
            EpicApplication.imageCache[EpicApplication.imgType].set(pieces[0],dates);
          }else{
            console.log("else");
              if(EpicApplication.imgList===0){
                let template_cloneNoImage=template.content.cloneNode(true).querySelector("li");
                template_cloneNoImage.textContent="No images found for this date"
                ul.appendChild(template_cloneNoImage);
              }
              objList=obj;
              for(let obj in EpicApplication.imageCache[EpicApplication.imgType].get(imgDate)) {
                EpicApplication.imgList.push(objList[obj].image);
                let template_cloneLi=template.content.cloneNode(true).querySelector("li");
  
                let template_cloneSpan=template.content.cloneNode(true).querySelector("li").querySelector("span");
                template_cloneSpan.textContent=`${imgDate} ${EpicApplication.imageCache[EpicApplication.imgType].get(imgDate)[obj]}`;
  
                template_cloneSpan.setAttribute("data-index",obj);
                ul.appendChild(template_cloneLi);
                template_cloneLi.appendChild(template_cloneSpan);
              }
          }
        })
        .catch(err=> {
          let template_cloneLi=template.content.cloneNode(true).querySelector("li");

          let template_cloneSpan=template.content.cloneNode(true).querySelector("li").querySelector("span");
          template_cloneSpan.textContent="Error";
          ul.appendChild(template_cloneLi);
          template_cloneLi.appendChild(template_cloneSpan);
        })
      
         
      })
    
      ul.addEventListener("click", function(e) {
        if(e.target.tagName==="SPAN"){
          let footerDate=document.querySelector("#earth-image-date");
          let footerTitle=document.querySelector("#earth-image-title");
          let imgDate2=document.querySelector("#date").value;
          let newDate=imgDate2.replaceAll("-","/");
          let img=document.getElementById("earth-image");
          img.src=`https://epic.gsfc.nasa.gov/archive/${EpicApplication.imgType}/${newDate}/jpg/${EpicApplication.imgList[e.target.dataset.index]}.jpg`;
          footerDate.textContent=objList[e.target.dataset.index].date;
          footerTitle.textContent=objList[e.target.dataset.index].caption;
        }
      })

      select.addEventListener("change", function(e) {
        EpicApplication.imgType=document.querySelector("#type").value;
        const existingDates = document.getElementById("image-menu").querySelectorAll("li");
        if (existingDates) {
          ul.textContent="";
        }
        fetch(`https://epic.gsfc.nasa.gov/api/${EpicApplication.imgType}/all`, {})
        .then(response => {  
          if(!response.ok) {
            throw new Error("Not 2xx response", {cause: response})
          }
          return response.json();
        })
        .then( obj => {
          allDates=obj;
          let date=document.querySelector("#date");
          date.max=allDates[0].date;
          //EpicApplication[EpicApplication.imgType]=allDates[0].date;
          
         })
        })

      date.addEventListener("change", function(e) {
        ul.textContent="";
      }) 
  })

})();